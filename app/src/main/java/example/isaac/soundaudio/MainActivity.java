package example.isaac.soundaudio;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;

import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {

    MediaPlayer mediaPlayer;
    Button playBtn, pauseBtn, restartBtn;
    SeekBar volumeController, placeController;
    AudioManager audioManager;
    int length;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mediaPlayer = MediaPlayer.create(this, R.raw.sad);
        volumeController = (SeekBar) findViewById(R.id.skb_one);
        placeController = (SeekBar) findViewById(R.id.skb_two);
        volumeControl();
        placeControl();
    }

    public void volumeControl() {
        audioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
        int maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        int currentVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        volumeController.setMax(maxVolume);
        volumeController.setProgress(currentVolume);

        volumeController.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, i, 0);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    public void placeControl() {
        mediaPlayer = MediaPlayer.create(this, R.raw.sad);
        placeController.setMax(mediaPlayer.getDuration());


        placeController.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                seekBar.getProgress();
                length = mediaPlayer.getCurrentPosition();
                mediaPlayer.getCurrentPosition();
                if (b) {
                    mediaPlayer.seekTo(i);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        new Timer().scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                placeController.setProgress(mediaPlayer.getCurrentPosition());
                if (mediaPlayer.isPlaying()) {
                    mediaPlayer.setLooping(true);
                }
            }
        }, 0, 1000);
    }

    public void play(View view) {

        mediaPlayer = MediaPlayer.create(this, R.raw.sad);
        mediaPlayer.start();
        mediaPlayer.seekTo(length);

        if (mediaPlayer.isPlaying()) {
            playBtn = findViewById(R.id.btn_one);
            playBtn.setEnabled(false);
        } else {
            playBtn.setEnabled(true);
        }
    }

    public void pause(View view) {
        mediaPlayer.stop();
        length = mediaPlayer.getCurrentPosition();
        mediaPlayer.getCurrentPosition();

        if (!mediaPlayer.isPlaying()) {
            playBtn.setEnabled(true);
        }
    }

    public void restart(View view) {
        if (mediaPlayer.isPlaying()) {
            playBtn.setEnabled(false);
            mediaPlayer.reset();
            mediaPlayer = MediaPlayer.create(this, R.raw.sad);
            mediaPlayer.start();
        } else {
            playBtn.setEnabled(false);
            mediaPlayer.reset();
            mediaPlayer = MediaPlayer.create(this, R.raw.sad);
            mediaPlayer.start();
        }
    }
}
